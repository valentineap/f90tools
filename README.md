# README #

This repository contains various Fortran modules that are likely to be of general use. Some modules are targeted towards geophysical/seismological users.

* mod_log       - Provides a framework for generating log files and displaying program output
* mod_types     - Provides various compound type definitions and associated functions
* mod_foptparse - A pure Fortran parser for command line options, loosely based around upon Python's optparse module
* mod_ndk       - Read records from .ndk file format, as used by the Global CMT catalogue (www.globalcmt.org)

See individual files for usage information.

### Conditions of use ###

You are welcome to make use of this code (as set out in the LICENSE file). In addition, please note the following flavours of *caveat emptor*:

* Any use of this code is at your own risk, and you are responsible for satisfying yourself that it functions as intended.

* My priorities lie in scientific research, *not* in software development. This code is written to suit the needs of my research; these may not be the same as your needs. Please do not complain about this.

* I do not guarantee that interfaces/implementation details will remain unchanged in any future updates.

* Yes, there are undoubtedly better ways to do X. Maybe I had a good reason for doing it this way. Maybe I was just being dumb. If you'd prefer it done a different way, then *code it yourself*.

* By all means contact me if you encounter a bug, or have a suggestion. However, I do not promise to fix/implement it, particularly if it is not relevant to my own work. Sorry.

I'm always pleased to hear from anyone who has found this code useful, and to receive notification of any publications that ensue. 

----

Andrew Valentine - Universiteit Utrecht - June 2014
* Email: a.p.valentine@uu.nl
* Website: http://www.geo.uu.nl/~andrew

----
